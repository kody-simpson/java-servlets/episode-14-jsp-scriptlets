<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<!DOCTYPE html>
<html>
<head>
    <title>JSP Scriptlets</title>
</head>
<body>

<h1>JSP Scriptlet Tags</h1>
<p>These are how you inject multiple lines of Java code into the JSP to do things like if statements, call methods, or whatever you need.</p>

<br>

<%-- This is how you do a JSP comment --%>
<%
    double myTestScore = 100.0;
    myTestScore = myTestScore - 50.0;
%>
<%-- You can mix different types of JSP tags together --%>
<p>My real test score: <%= myTestScore %></p>

<%if (1 + 1 == 2){ %>
        <p>One plus one does indeed equal two<p>;
    <%}else{ %>
        <p>you stoopid</p>
    <%}%>

</body>
</html>